﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using StoreApp.Domains;
using StoreApp.ExternalServices;
using StoreApp.Models;

namespace StoreApp.Service
{
    public interface ISupplierService
    {

        void UploadItem(UploadItemViewModel model, string userId);
        List<Item> GetAllSupplierItems(string userId);
    }
}