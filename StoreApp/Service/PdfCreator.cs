﻿using System.Globalization;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.Ajax.Utilities;
using StoreApp.Domains;

namespace StoreApp.Service
{
    public class PdfCreator
    {
        public static FileStream CreatePdf(Invoice invoice)
        {
            FileStream fileStream = new FileStream(invoice.Order.OrderId+".pdf", FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();
            PdfWriter writer = PdfWriter.GetInstance(document, fileStream);
            document.Open();
            PdfPTable table = new PdfPTable(3);
            PdfPCell cell = new PdfPCell(new Phrase("Item Listings and Price"));
            cell.Colspan = 3;
            cell.HorizontalAlignment = 1;
            table.AddCell(cell);
            
            invoice.InvoiceItems.ForEach(item =>
            {
                table.AddCell(item.Item.ItemName);
                table.AddCell(item.Item.ItemPrice.ToString(CultureInfo.CurrentCulture));
                table.AddCell(item.Item.Quantity.ToString());
            });

            document.Add(table);
            document.Close();
            return fileStream;
        }
    }
}