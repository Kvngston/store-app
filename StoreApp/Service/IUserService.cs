﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using StoreApp.Domains;
using StoreApp.Models;
using static System.Web.HttpContext;

namespace StoreApp.Service
{
    public interface IUserService
    {
        Task<Tuple<ApplicationUser, bool, IEnumerable<string>>> CreateUser(CustomerViewModel customerViewModel = null,
            StaffViewModel staffViewModel = null, SupplierViewModel supplierViewModel = null);
    }
}