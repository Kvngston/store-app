﻿using System.Collections.Generic;
using System.Linq;
using StoreApp.Domains;
using StoreApp.Models;
using StoreApp.Service.ServiceImpl;

namespace StoreApp.Service
{
    public interface IStaffService
    {

        List<AuditTrail> GetAllAuditTrails(string userId);
        List<Order> GetAllOrders(string userId);
        List<Invoice> GetAllInvoices(string userId);
        List<Customer> GetAllCustomers(string userId);
        
    }
}