﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Microsoft.Ajax.Utilities;
using StoreApp.Domains;
using StoreApp.Models;

namespace StoreApp.Service
{
    public interface IItemService
    {

        void SeedInitialItems();
        List<Item> GetAllItems();
        Cart OpenCart(string userId);
        Cart GetUserCart(string userId);

        void AddToCart(string userId, int itemId);

        void IncreaseQuantity(string userId, int itemId);

        void DecreaseQuantity(string userId, int itemId);

        void RemoveFromCart(string userId, int itemId);

        Invoice Checkout(string userId);
        
    }
}