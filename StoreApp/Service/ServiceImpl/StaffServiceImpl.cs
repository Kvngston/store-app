﻿using System.Collections.Generic;
using System.Linq;
using StoreApp.Domains;
using StoreApp.Models;

namespace StoreApp.Service.ServiceImpl
{
    public class StaffServiceImpl : IStaffService
    {
        private ApplicationDbContext db;
        private IAuditTrailService _auditTrailService;

        public StaffServiceImpl(IAuditTrailService auditTrailService, ApplicationDbContext dbContext)
        {
            db = dbContext;
            _auditTrailService = auditTrailService;
        }
        
        // public IAuditTrailService AuditTrailService
        // {
        //     get
        //     {
        //         return _auditTrailService ?? new AuditTrailServiceImpl();
        //     }
        //     set
        //     {
        //         _auditTrailService = value;
        //     }
        // }

        public List<AuditTrail> GetAllAuditTrails(string userId)
        {
            _auditTrailService.CreateAuditTrail(userId, "Queried all Audit Logs");
            return db.AuditTrails.ToList();
        }

        public List<Order> GetAllOrders(string userId)
        {
            _auditTrailService.CreateAuditTrail(userId, "Queried all User Orders");
            return db.Orders.ToList();
        }

        public List<Invoice> GetAllInvoices(string userId)
        {
            _auditTrailService.CreateAuditTrail(userId, "Queried all User Invoices");
            return db.Invoices.ToList();
        }

        public List<Customer> GetAllCustomers(string userId)
        {
            _auditTrailService.CreateAuditTrail(userId, "Queried all Customers");
            return db.Customers.ToList();
        }
    }
}