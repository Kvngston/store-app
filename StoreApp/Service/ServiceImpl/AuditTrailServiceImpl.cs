﻿using System;
using StoreApp.Domains;
using StoreApp.Models;

namespace StoreApp.Service.ServiceImpl
{
    public class AuditTrailServiceImpl: IAuditTrailService
    {
        private ApplicationDbContext db;
        
        public AuditTrailServiceImpl(ApplicationDbContext dbContext)
        {
            db = dbContext;
        }
        public void CreateAuditTrail(string userId, string action)
        {
            var auditTrail = new AuditTrail()
            {
                UserId = userId,
                ActionDetails = action,
                TimeOfAction = DateTime.Now
            };

            db.AuditTrails.Add(auditTrail);
            db.SaveChanges();
        }

    }
}