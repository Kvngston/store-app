﻿using System.Threading.Tasks;
using System.Web;
using static System.Web.HttpContext;
using System.Web.ApplicationServices;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace StoreApp.Service.ServiceImpl
{
    public class RoleServiceImpl : IRoleService
    {
        private static RoleManager<IdentityRole> _roleManager;

        public RoleServiceImpl(RoleManager<IdentityRole> roleManager)
        {
            _roleManager = roleManager;
        }
        
        // public RoleManager<IdentityRole> RoleManager
        // {
        //     get
        //     {
        //         return _roleManager ?? new RoleManager<IdentityRole>(new RoleStore<IdentityRole>());
        //     }
        //     private set 
        //     { 
        //         _roleManager = value; 
        //     }
        // }

        public async Task<bool> CreateRole(string roleName)
        {
            var role = new IdentityRole(roleName);

            if (await _roleManager.RoleExistsAsync(roleName))
            {
                return true;
            }
            
            var result = await _roleManager.CreateAsync(role);
            return result.Succeeded;
        }
    }
}