﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using StoreApp.Domains;
using StoreApp.Models;
using static System.Web.HttpContext;

namespace StoreApp.Service.ServiceImpl
{
    public class UserServiceImpl : IUserService
    {
        private ApplicationUserManager _manager;
        private ApplicationDbContext db;
        private IAuditTrailService _auditTrailService;
        private IRoleService _roleService;

        public UserServiceImpl(ApplicationUserManager userManager, IAuditTrailService auditTrailService, IRoleService roleService, ApplicationDbContext dbContext)
        {
            db = dbContext;
            _manager = userManager;
            _auditTrailService = auditTrailService;
            _roleService = roleService;
        }

        // public ApplicationUserManager UserManager
        // {
        //     get { return _manager ?? Current.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
        //     set { _manager = value; }
        // }
        //
        // public IAuditTrailService AuditTrailService
        // {
        //     get
        //     {
        //         return _auditTrailService ?? new AuditTrailServiceImpl();
        //     }
        //     set
        //     {
        //         _auditTrailService = value;
        //     }
        // }
        // public IRoleService RoleService
        // {
        //     get
        //     {
        //         return _roleService ?? new RoleServiceImpl();
        //     }
        //     set
        //     {
        //         _roleService = value;
        //     }
        // }
        
        public async Task<Tuple<ApplicationUser, bool, IEnumerable<string>>> CreateUser(CustomerViewModel customerViewModel = null,
            StaffViewModel staffViewModel = null, SupplierViewModel supplierViewModel = null)
        {
            if (customerViewModel != null)
            {
                var user = new ApplicationUser()
                {
                    Email = customerViewModel.Email,
                    UserName = customerViewModel.Email,
                    PhoneNumber = customerViewModel.PhoneNumber
                };

                var userResult = await _manager.CreateAsync(user, customerViewModel.Password);

                if (!userResult.Succeeded)
                {
                    return new Tuple<ApplicationUser, bool, IEnumerable<string>>(null, false, userResult.Errors);
                }
                
                var customer = new Customer()
                {
                    FirstName = customerViewModel.FirstName,
                    LastName = customerViewModel.LastName,
                    Address = customerViewModel.Address,
                    UserId = user.Id
                };


                db.Customers.Add(customer);
                await db.SaveChangesAsync();

                
                var roleResult = await _roleService.CreateRole("CUSTOMER");
                _auditTrailService.CreateAuditTrail(user.Id, "Created new Role");

                if (roleResult)
                {
                    await _manager.AddToRoleAsync(user.Id, "CUSTOMER");
                    _auditTrailService.CreateAuditTrail(user.Id, "Created New Customer");
                    return new Tuple<ApplicationUser, bool, IEnumerable<string>>(user, true, null);
                }

                return new Tuple<ApplicationUser, bool, IEnumerable<string>>(null, false, null);
            }

            if (staffViewModel != null)
            {
                var user = new ApplicationUser()
                {
                    Email = staffViewModel.Email,
                    UserName = staffViewModel.Email,
                    PhoneNumber = staffViewModel.PhoneNumber
                };
                var userResult = await _manager.CreateAsync(user, "Default1234_");
                
                if (!userResult.Succeeded)
                {
                    return new Tuple<ApplicationUser, bool, IEnumerable<string>>(null, false, userResult.Errors);
                }
                
                var staff = new Staff()
                {
                    FirstName = staffViewModel.FirstName,
                    LastName = staffViewModel.LastName,
                    StaffId = "STF" + DateTime.Now.Year + (db.Staffs.ToList().Count + 1),
                    UserId = user.Id
                };
                db.Staffs.Add(staff);
                await db.SaveChangesAsync();

                var roleResult = await _roleService.CreateRole("STAFF");

                if (roleResult)
                {
                    await _manager.AddToRoleAsync(user.Id, "STAFF");

                    await NotificationRequest.SendMail(user.Email,
                        $"Your Account has been opened. Your Credentials are Email: {user.Email}, Password: 'Default1234_'. You can proceed to login and change your password with this details.",
                        "Account Opened");

                    _auditTrailService.CreateAuditTrail(user.Id, "Created New Staff");
                    _auditTrailService.CreateAuditTrail(user.Id, "Sent Details email to new Staff");
                    return new Tuple<ApplicationUser, bool, IEnumerable<string>>(user, true,null);
                }

                return new Tuple<ApplicationUser, bool, IEnumerable<string>>(null, false, null);
            }

            if (supplierViewModel != null)
            {
                var user = new ApplicationUser()
                {
                    Email = supplierViewModel.Email,
                    UserName = supplierViewModel.Email,
                    PhoneNumber = supplierViewModel.PhoneNumber
                };
                var userResult = await _manager.CreateAsync(user, supplierViewModel.Password);
                if (!userResult.Succeeded)
                {
                    return new Tuple<ApplicationUser, bool, IEnumerable<string>>(null, false, userResult.Errors);
                }
                
                var supplier = new Supplier()
                {
                    SupplierName = supplierViewModel.SupplierName,
                    UserId = user.Id,
                    SupplierItems = new List<Item>()
                };

                db.Suppliers.Add(supplier);
                await db.SaveChangesAsync();
                
                var roleResult = await _roleService.CreateRole("SUPPLIER");
                _auditTrailService.CreateAuditTrail(user.Id, "Created New Role");

                if (roleResult)
                {
                    await _manager.AddToRoleAsync(user.Id, "SUPPLIER");

                    _auditTrailService.CreateAuditTrail(user.Id, "New Supplier Created");
                    return new Tuple<ApplicationUser, bool, IEnumerable<string>>(user, true, null);
                }
                return new Tuple<ApplicationUser, bool, IEnumerable<string>>(null, false, null);
            }


            return new Tuple<ApplicationUser, bool, IEnumerable<string>>(null, false, null);
        }
    }
}