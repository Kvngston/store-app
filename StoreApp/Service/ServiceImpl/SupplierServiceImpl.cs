﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using StoreApp.Domains;
using StoreApp.ExternalServices;
using StoreApp.Models;

namespace StoreApp.Service.ServiceImpl
{
    public class SupplierServiceImpl : ISupplierService
    {
         private static ApplicationDbContext db;
        private IAuditTrailService _auditTrailService;
        private CloudinaryInitialization _cloudinaryInitialization;

        public SupplierServiceImpl(IAuditTrailService auditTrailService, ApplicationDbContext dbContext, CloudinaryInitialization cloudinaryInitialization)
        {
            _auditTrailService = auditTrailService;
            db = dbContext;
            _cloudinaryInitialization = cloudinaryInitialization;
        }

        public void UploadItem(UploadItemViewModel model, string userId)
        {
            var cloudinary = _cloudinaryInitialization.Initialize();
            
            var imageParams = new ImageUploadParams()
            {
                File = new FileDescription(model.File.FileName, model.File.InputStream)
            };

            var uploadResult = cloudinary.Upload(imageParams);
            var item = new Item()
            {
                Quantity = model.Quantity,
                ExpiryDate = model.ExpiryDate,
                ItemName = model.ItemName,
                ItemPrice = model.ItemPrice,
                ItemImageLocation = uploadResult.SecureUrl.AbsoluteUri,
            };

            var chosenCategory =  db.Categories.FirstOrDefault(category => category.CategoryName == model.Category);
            if (chosenCategory == null)
            {
                chosenCategory = new Category()
                {
                    CategoryName = model.Category,
                    CategoryItems = new List<Item>()
                };
                db.Categories.Add(chosenCategory);
                db.SaveChanges();
            }

            item.Category = chosenCategory;
            db.Items.Add(item);
            chosenCategory.CategoryItems.Add(item);

            db.Entry(chosenCategory).State = EntityState.Modified;
            

            var supplier = db.Suppliers.FirstOrDefault(supplier1 => supplier1.UserId == userId);
            if (supplier == null)
            {
                throw new Exception("Supplier Not Found");
            }
            
            supplier.SupplierItems.Add(item);
            db.Entry(supplier).State = EntityState.Modified;
            db.SaveChanges();
            _auditTrailService.CreateAuditTrail(userId, "Uploaded new Item");
        }

        public List<Item> GetAllSupplierItems(string userId)
        {
            var supplier = db.Suppliers.FirstOrDefault(supplier1 => supplier1.UserId == userId);
            if (supplier == null)
            {
                throw new Exception("Supplier Not Found");
            }

            _auditTrailService.CreateAuditTrail(userId, "Queried all Supplier Items");

            List<Item> items = supplier.SupplierItems.ToList();

            return items ?? new List<Item>();

        }
    }
}