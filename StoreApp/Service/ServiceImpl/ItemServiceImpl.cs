﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Microsoft.Ajax.Utilities;
using StoreApp.Domains;
using StoreApp.Models;

namespace StoreApp.Service.ServiceImpl
{
    public class ItemServiceImpl : IItemService
    {
        private ApplicationDbContext db;
        private IAuditTrailService _auditTrailService;
        public ItemServiceImpl(ApplicationDbContext dbContext, IAuditTrailService auditTrailService)
        {
            db = dbContext;
            _auditTrailService = auditTrailService;
        }

        public void SeedInitialItems()
        {
            var technology = new Category()
            {
                CategoryName = "Technology",
            };
            
            var furniture = new Category()
            {
                CategoryName = "Furniture"
            };
            
            var food = new Category()
            {
                CategoryName = "Food"
            };
            var medicine = new Category()
            {
                CategoryName = "Medicine"
            };
            var gadgets = new Category()
            {
                CategoryName = "Gadgets"
            };

            var categories = new List<Category>(){technology,furniture,food,medicine,gadgets};
            db.Categories.AddRange(categories);
            db.SaveChanges();

            var technologyItems = new List<Item>()
            {
                new Item()
                {
                    ItemName = "Google Pixel",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 500000,
                    Category = technology,
                    ItemImageLocation = ""
                },
                new Item()
                {
                    ItemName = "IPhone XSMax",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 500000,
                    Category = technology,
                    ItemImageLocation = ""
                },
                new Item()
                {
                    ItemName = "MacBook Pro",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 1000000,
                    Category = technology,
                    ItemImageLocation = ""
                },
                new Item()
                {
                    ItemName = "Alienware",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 1500000,
                    Category = technology,
                    ItemImageLocation = ""
                },
                new Item()
                {
                    ItemName = "IPad pro",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 500000,
                    Category = technology,
                    ItemImageLocation = ""
                }
            };

            technology.CategoryItems = technologyItems;
            db.Entry(technology).State = EntityState.Modified;
            
            var furnitureItems = new List<Item>()
            {
                new Item()
                {
                    ItemName = "Plastic Chair",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 5000,
                    Category = furniture,
                    ItemImageLocation = ""
                },
                new Item()
                {
                    ItemName = "Plastic Table",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 7500,
                    Category = furniture,
                    ItemImageLocation = ""
                },
                new Item()
                {
                    ItemName = "Sofa",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 100000,
                    Category = furniture,
                    ItemImageLocation = ""
                },
                new Item()
                {
                    ItemName = "Side stools",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 10000,
                    Category = furniture,
                    ItemImageLocation = ""
                },
                new Item()
                {
                    ItemName = "BedFrames",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 500000,
                    Category = furniture,
                    ItemImageLocation = ""
                }
            };

            furniture.CategoryItems = furnitureItems;
            db.Entry(furniture).State = EntityState.Modified;
            
            var foodItems = new List<Item>()
            {
                new Item()
                {
                    ItemName = "Corn Flakes",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 1400,
                    Category = food,
                    ItemImageLocation = ""
                },
                new Item()
                {
                    ItemName = "Dano Milk",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 900,
                    Category = food,
                    ItemImageLocation = ""
                },
                new Item()
                {
                    ItemName = "Bag of Rice",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 10000,
                    Category = food,
                    ItemImageLocation = ""
                },
                new Item()
                {
                    ItemName = "Yam",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 950,
                    Category = food,
                    ItemImageLocation = ""
                },
                new Item()
                {
                    ItemName = "Milo",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 800,
                    Category = food,
                    ItemImageLocation = ""
                }
            };

            food.CategoryItems = foodItems;
            db.Entry(food).State = EntityState.Modified;
            
            var medicineItems = new List<Item>()
            {
                new Item()
                {
                    ItemName = "Panadol",
                    Quantity = 100,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 100,
                    Category = medicine,
                    ItemImageLocation = ""
                },
                new Item()
                {
                    ItemName = "Vitamin C",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 500,
                    Category = medicine,
                    ItemImageLocation = ""
                },
                new Item()
                {
                    ItemName = "Multivitamins",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 1000,
                    Category = medicine,
                    ItemImageLocation = ""
                },
                new Item()
                {
                    ItemName = "First Aid Kit",
                    Quantity = 200,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 5000,
                    Category = medicine,
                    ItemImageLocation = ""
                },
                new Item()
                {
                    ItemName = "Cough Syrup",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 1500,
                    Category = medicine,
                    ItemImageLocation = ""
                }
            };

            medicine.CategoryItems = medicineItems;
            db.Entry(medicine).State = EntityState.Modified;
            
            var gadgetItems = new List<Item>()
            {
                new Item()
                {
                    ItemName = "Headphones",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 45000,
                    Category = gadgets,
                    ItemImageLocation = ""
                },
                new Item()
                {
                    ItemName = "OTG",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 7000,
                    Category = gadgets,
                    ItemImageLocation = ""
                },
                new Item()
                {
                    ItemName = "Air Pods",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 25000,
                    Category = gadgets,
                    ItemImageLocation = ""
                },
                new Item()
                {
                    ItemName = "Laptop Stand",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 10000,
                    Category = gadgets,
                    ItemImageLocation = ""
                },
                new Item()
                {
                    ItemName = "Wireless Charger",
                    Quantity = 20,
                    ExpiryDate = new DateTime(2025, 12, 30),
                    ItemPrice = 5000,
                    Category = gadgets,
                    ItemImageLocation = ""
                }
            };

            gadgets.CategoryItems = gadgetItems;
            db.Entry(gadgets).State = EntityState.Modified;
            
            db.Items.AddRange(technologyItems);
            db.Items.AddRange(furnitureItems);
            db.Items.AddRange(foodItems);
            db.Items.AddRange(medicineItems);
            db.Items.AddRange(gadgetItems);
            db.SaveChanges();
        }

        public List<Item> GetAllItems()
        {
            return db.Items.ToList();
        }

        public Cart OpenCart(string userId)
        {
            var customer = db.Customers.FirstOrDefault(x => x.UserId == userId);

            if (customer == null)
            {
                throw new Exception("Customer Not Found");
            }

            Cart cart = db.Carts.Include("CartItems").ToList().FirstOrDefault(x => x.CustomerId == customer.Id);
            if (cart == null)
            {
                cart = new Cart()
                {
                    Customer = customer,
                    CartItems = new List<OrderItem>()
                };
                db.Carts.Add(cart);
                db.SaveChanges();
                _auditTrailService.CreateAuditTrail(userId, "Created Cart For User");
                return cart;
            }
            
            _auditTrailService.CreateAuditTrail(userId, "Returned User Cart For User");
            return cart;
        }
        
        public Cart GetUserCart(string userId)
        {
            var customer = db.Customers.FirstOrDefault(x => x.UserId == userId);

            if (customer == null)
            {
                throw new Exception("Customer Not Found");
            }
            
            _auditTrailService.CreateAuditTrail(userId, "Queried User Cart");
            return db.Carts.FirstOrDefault(x => x.CustomerId == customer.Id);
        }
        
        public void AddToCart(string userId, int itemId)
        {

            var item = db.Items.FirstOrDefault(x => x.Id == itemId);
            var userCart = GetUserCart(userId);
            
            if (item == null)
            {
                throw new Exception("Item not found");
            }

            var theItem = userCart.CartItems.FirstOrDefault(x => x.ItemId == itemId);
            
            if (theItem == null)
            {
                var orderItem = new OrderItem()
                {
                    Item = item,
                    Cart = userCart,
                    Quantity = 1
                };

                db.OrderItems.Add(orderItem);
                userCart.CartItems.Add(orderItem);
            }
            else
            {
                theItem.Quantity++;
                db.Entry(theItem).State = EntityState.Modified;
            }

            db.Entry(userCart).State = EntityState.Modified;
            db.SaveChanges();
            _auditTrailService.CreateAuditTrail(userId, "User Added item to Cart");

        }
        
        public void IncreaseQuantity(string userId, int itemId)
        {
            var cart = GetUserCart(userId);
            var item = cart.CartItems.FirstOrDefault(x => x.Id == itemId);

            if (item == null)
            {
                throw new Exception("Item not found");
            }

            item.Quantity++;

            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();
        }
        
        public void DecreaseQuantity(string userId, int itemId)
        {
            var cart = GetUserCart(userId);
            var item = cart.CartItems.FirstOrDefault(x => x.Id == itemId);

            if (item == null)
            {
                throw new Exception("Item not found");
            }

            if (item.Quantity != 0)
            {
                item.Quantity--;
            }

            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();
        }
        
        public void RemoveFromCart(string userId, int itemId)
        {
            var cart = GetUserCart(userId);
            var item = cart.CartItems.FirstOrDefault(x => x.Id == itemId);

            if (item == null)
            {
                throw new Exception("item not found");
            }
            cart.CartItems.Remove(item);

            db.Entry(cart).State = EntityState.Modified;
            db.SaveChanges();
            _auditTrailService.CreateAuditTrail(userId, "User Removed item From Cart");
        }
        
        public  Invoice Checkout(string userId)
        {
            var cart = GetUserCart(userId);
            
            decimal totalPrice = 0;
            cart.CartItems.ForEach(item =>
            {
                var original = item.Item;
                var result = original.Quantity - item.Quantity;
                totalPrice += item.Quantity * original.ItemPrice;
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
            });

            var order = new Order()
            {
                UserId = userId,
                CreatedAt = DateTime.Now,
                OrderId = Guid.NewGuid().ToString(),
                TotalPrice = totalPrice
            };

            db.Orders.Add(order);
            _auditTrailService.CreateAuditTrail(userId, "Created new Order");
            
            var invoice = new Invoice()
            {
                UserId = userId,
                CreatedAt = DateTime.Now,
                InvoiceItems = cart.CartItems,
                TotalPrice = totalPrice,
                Order = order
            };

            db.Invoices.Add(invoice);
            db.Entry(order).State = EntityState.Modified;
            _auditTrailService.CreateAuditTrail(userId, "Created New Invoice");
            db.SaveChanges();

            return invoice;

            // delete from supplier's page too
        }
    }
}