﻿using System;
using StoreApp.Domains;
using StoreApp.Models;

namespace StoreApp.Service
{
    public interface IAuditTrailService
    {
        void CreateAuditTrail(string userId, string action);
    }
}