﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace StoreApp.Service
{
    public class NotificationRequest
    {
        public static async Task SendMail(string email, string messageBody, string messageSubject)
        {
            try
            {
                MailMessage mailMessage = new MailMessage()
                {
                    To = { email},
                    Body = messageBody,
                    From = new MailAddress("storesupport@gmail.com"),
                    Subject = messageSubject
                };
                await GetClient().SendMailAsync(mailMessage);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        
        public static async Task SendMailWithAttachment(string email, string messageBody, string messageSubject, Attachment attachment)
        {
            try
            {
                MailMessage mailMessage = new MailMessage()
                {
                    To = { email},
                    Body = messageBody,
                    From = new MailAddress("storesupport@gmail.com"),
                    Subject = messageSubject,
                    Attachments = { attachment}
                };
                
                await GetClient().SendMailAsync(mailMessage);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public static SmtpClient GetClient()
        {
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("tochukwuchinedu007@gmail.com", "Currency12345_");
            return client;
        }
        
    }
}