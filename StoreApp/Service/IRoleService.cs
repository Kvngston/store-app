﻿using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using StoreApp.Models;
using StoreApp.Service.ServiceImpl;

namespace StoreApp.Service
{
    public interface IRoleService
    {
        Task<bool> CreateRole(string roleName);
    }
}