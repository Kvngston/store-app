﻿using Ninject.Web.Mvc;
using StoreApp.Infrastructure;
using static System.Web.Mvc.DependencyResolver;
using NinjectDependencyResolver = StoreApp.Infrastructure.NinjectDependencyResolver;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(StoreApp.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(StoreApp.App_Start.NinjectWebCommon), "Stop")]


namespace StoreApp.App_Start
{
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject;
    using Ninject.Web.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    public class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        private static void RegisterServices(IKernel kernel)
        {
            //System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver =
            //    new Ninject.Web.WebApi.NinjectDependencyResolver(kernel);
            
            SetResolver(new NinjectDependencyResolver(kernel));

        }
    }
}