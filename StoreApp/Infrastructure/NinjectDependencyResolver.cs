﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Ninject;
using StoreApp.ExternalServices;
using StoreApp.Models;
using StoreApp.Service;
using StoreApp.Service.ServiceImpl;
using static System.Web.HttpContext;

namespace StoreApp.Infrastructure
{
    public class NinjectDependencyResolver :IDependencyResolver
    {
        private IKernel Kernel;

        public NinjectDependencyResolver(IKernel kernel)
        {
            Kernel = kernel;
            AddBindings();
        }

        private void AddBindings()
        {
            Kernel.Bind<IUserStore<ApplicationUser>>().To<UserStore<ApplicationUser>>();
            Kernel.Bind<ApplicationUserManager>().ToSelf();
            Kernel.Bind<IAuthenticationManager>().ToMethod(x => Current.GetOwinContext().Authentication);
            Kernel.Bind<ApplicationSignInManager>().ToSelf();
            Kernel.Bind<RoleManager<IdentityRole>>().ToSelf();
            Kernel.Bind<ApplicationDbContext>().ToSelf();
            Kernel.Bind<IAuditTrailService>().To<AuditTrailServiceImpl>();
            Kernel.Bind<IItemService>().To<ItemServiceImpl>();
            Kernel.Bind<IRoleService>().To<RoleServiceImpl>();
            Kernel.Bind<IStaffService>().To<StaffServiceImpl>();
            Kernel.Bind<ISupplierService>().To<SupplierServiceImpl>();
            Kernel.Bind<IUserService>().To<UserServiceImpl>();
            Kernel.Bind<CloudinaryInitialization>().ToSelf();
            Kernel.Bind<NotificationRequest>().ToSelf();
            Kernel.Bind<PdfCreator>().ToSelf();
        }

        public object GetService(Type serviceType) => Kernel.TryGet(serviceType);


        public IEnumerable<object> GetServices(Type serviceType) => Kernel.GetAll(serviceType);
    }
}