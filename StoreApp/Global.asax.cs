using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Ninject;
using StoreApp.Infrastructure;
using StoreApp.Jobs;
using StoreApp.Models;
using StoreApp.Service;

namespace StoreApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            
            IKernel kernel = new StandardKernel();
            NinjectDependencyResolver resolver = new NinjectDependencyResolver(kernel);
            Database.SetInitializer(new InitializationStrategy(resolver.GetService<IItemService>(),
                resolver.GetService<ApplicationUserManager>(), resolver.GetService<RoleManager<IdentityRole>>()));
            PurgeExpiredItems.Purge();
        }
    }
}
