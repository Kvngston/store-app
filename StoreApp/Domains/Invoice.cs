﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using StoreApp.Models;

namespace StoreApp.Domains
{
    public class Invoice
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        public decimal TotalPrice { get; set; }

        public DateTime CreatedAt { get; set; }
        
        public int OrderId { get; set; }
        public Order Order { get; set; }
        
        public ICollection<OrderItem> InvoiceItems { get; set; }
    }
}