﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreApp.Domains
{
    public class Item
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public string ItemName { get; set; }
        
        public decimal ItemPrice { get; set; }
        
        public int Quantity { get; set; }

        public DateTime ExpiryDate { get; set; }

        public string ItemImageLocation { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}