﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreApp.Domains
{
    public class Category
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public string CategoryName { get; set; }
        
        [ForeignKey("CategoryId")]
        public virtual List<Item> CategoryItems { get; set; } = new List<Item>();
    }
}