﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using StoreApp.Models;

namespace StoreApp.Domains
{
    public class AuditTrail
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        public string ActionDetails { get; set; }
        
        public DateTime TimeOfAction { get; set; }
    }
    
}