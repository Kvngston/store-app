﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreApp.Domains
{
    public class OrderItem
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public int Quantity { get; set; }

        public int ItemId { get; set; }
        public Item Item { get; set; }

        public int? CartId { get; set; }
        public Cart Cart { get; set; }
    }
}