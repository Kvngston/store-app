﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using StoreApp.Models;
using StoreApp.Service;
using StoreApp.Service.ServiceImpl;
using static System.Web.HttpContext;

namespace StoreApp.Controllers
{
    [Authorize(Roles = "ADMIN")]
    public class AdminController : Controller
    {
        private ApplicationUserManager _manager;
        private ApplicationSignInManager _signInManager;
        private IUserService _userService;
        private IAuditTrailService _auditTrailService;
        
        
        public AdminController(){}
        public AdminController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, IUserService userService, IAuditTrailService auditTrailService)
        {
            _manager = userManager;
            _signInManager = signInManager;
            _userService = userService;
            _auditTrailService = auditTrailService;
        }

        // public ApplicationSignInManager SignInManager
        // {
        //     get { return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>(); }
        //     private set { _signInManager = value; }
        // }
        //
        // public ApplicationUserManager UserManager
        // {
        //     get
        //     {
        //         return _manager ?? Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
        //     }
        //     set
        //     {
        //         _manager = value;
        //     }
        // }
        // public IAuditTrailService AuditTrailService
        // {
        //     get
        //     {
        //         return _auditTrailService ?? new AuditTrailServiceImpl();
        //     }
        //     set
        //     {
        //         _auditTrailService = value;
        //     }
        // }
        // public IUserService UserService
        // {
        //     get
        //     {
        //         return _userService ?? new UserServiceImpl();
        //     }
        //     set
        //     {
        //         _userService = value;
        //     }
        // }

        public ActionResult HomePage()
        {
            return View();
        }

        //Create Staffs
        public ActionResult CreateStaff()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> CreateStaff(StaffViewModel staffViewModel)
        {
            var resultSet = await _userService.CreateUser(null, staffViewModel);
            if (resultSet.Item1 != null)
            {
                if (resultSet.Item2)
                {
                    return RedirectToAction("Index", "Home");
                }
            }else if (resultSet.Item1 == null && resultSet.Item3 != null)
            {
                ViewBag.HasError = true;
                ViewBag.Error = resultSet.Item3;
            }
            return View();
        }
    }
}