﻿using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using StoreApp.Service;
using static System.Web.HttpContext;

namespace StoreApp.Controllers
{
    [Authorize(Roles = "STAFF")]
    public class StaffController : Controller
    {
        private ApplicationUserManager _manager;
        private IStaffService _staffService;

        public StaffController(){}

        public StaffController(ApplicationUserManager userManager, IStaffService staffService)
        {
            _manager = userManager;
            _staffService = staffService;
        }

        // public ApplicationUserManager UserManager
        // {
        //     get { return _manager ?? Current.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
        //     set { _manager = value; }
        // }
        //
        // public IStaffService StaffService
        // {
        //     get
        //     {
        //         return _staffService ?? new StaffServiceImpl();
        //     }
        //     set
        //     {
        //         _staffService = value;
        //     }
        // }

        // GET
        public ActionResult HomePage()
        {
            return View();
        }

        //view Audit trails
        public ActionResult CheckAuditLogs()
        {
            var userId = Current.User.Identity.GetUserId();
            return View(_staffService.GetAllAuditTrails(userId));
        }

        //view Invoices 
        public ActionResult CheckAllInvoices()
        {
            var userId = Current.User.Identity.GetUserId();
            return View(_staffService.GetAllInvoices(userId));
        }

        //View Orders
        public ActionResult CheckOrders()
        {
            var userId = Current.User.Identity.GetUserId();
            return View(_staffService.GetAllOrders(userId));
        }

        public ActionResult CheckAllCustomers()
        {
            var userId = Current.User.Identity.GetUserId();
            return View(_staffService.GetAllCustomers(userId));
        }
    }
}