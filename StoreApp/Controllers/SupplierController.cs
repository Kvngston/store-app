﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using StoreApp.Models;
using static System.Web.HttpContext;
using StoreApp.Service;
using StoreApp.Service.ServiceImpl;

namespace StoreApp.Controllers
{
    [Authorize(Roles = "SUPPLIER")]
    public class SupplierController : Controller
    {
     
        private ApplicationUserManager _manager;
        private ApplicationSignInManager _signInManager;
        private IUserService _userService;
        private ISupplierService _supplierService;
        private IAuditTrailService _auditTrailService;
        
        public SupplierController(){}

        public SupplierController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, IUserService userService, IAuditTrailService auditTrailService, ISupplierService supplierService)
        {
            _manager = userManager;
            _signInManager = signInManager;
            _userService = userService;
            _auditTrailService = auditTrailService;
            _supplierService = supplierService;
        }

        // public ApplicationSignInManager SignInManager
        // {
        //     get { return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>(); }
        //     private set { __signInManager = value; }
        // }
        //
        // public ApplicationUserManager UserManager
        // {
        //     get
        //     {
        //         return _manager ?? Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
        //     }
        //     set
        //     {
        //         _manager = value;
        //     }
        // }
        //
        // public ISupplierService SupplierService
        // {
        //     get
        //     {
        //         return _supplierService ?? new SupplierServiceImpl();
        //     }
        //     set
        //     {
        //         _supplierService = value;
        //     }
        // }
        //
        // public IUserService UserService
        // {
        //     get
        //     {
        //         return _userService ?? new UserServiceImpl();
        //     }
        //     set
        //     {
        //         _userService = value;
        //     }
        // }
        //
        // public IAuditTrailService AuditTrailService
        // {
        //     get
        //     {
        //         return _auditTrailService ?? new AuditTrailServiceImpl();
        //     }
        //     set
        //     {
        //         _auditTrailService = value;
        //     }
        // }
        // GET

        public ActionResult UploadItem()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UploadItem(UploadItemViewModel model)
        {

            try
            {
                var userId = Current.User.Identity.GetUserId();
                _supplierService.UploadItem(model,userId);
                return View("HomePage", _supplierService.GetAllSupplierItems(userId));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                ViewBag.Error = e.Message;
                View("Error");
            }

            return View();
        }

        public ActionResult HomePage()
        {
            try
            {
                var userId = Current.User.Identity.GetUserId();
                return View(_supplierService.GetAllSupplierItems(userId));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                ViewBag.Error = e.Message;
                View("Error");
            }

            return View();
        }
        
        [AllowAnonymous]
        public ActionResult CreateSupplier()
        {
            return View();
        }
        
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> CreateSupplier(SupplierViewModel model)
        {

            var resultSet = await _userService.CreateUser(null, null, model);
            if (resultSet.Item1 != null)
            {
                
                if (resultSet.Item2)
                {
                    await _signInManager.SignInAsync(resultSet.Item1, false, false);
                    _auditTrailService.CreateAuditTrail(resultSet.Item1.Id, "User Logged In");
                    return View("HomePage", _supplierService.GetAllSupplierItems(resultSet.Item1.Id));
                }
            }else if (resultSet.Item1 == null && resultSet.Item3 != null)
            {
                ViewBag.HasError = true;
                ViewBag.Error = resultSet.Item3;
            }
            return View();
        }
    }
}