﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text.pdf;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using StoreApp.Domains;
using StoreApp.Models;
using StoreApp.Service;
using StoreApp.Service.ServiceImpl;
using static System.Web.HttpContext;

namespace StoreApp.Controllers
{
    [Authorize(Roles = "CUSTOMER")]
    public class CustomerController : Controller
    {
        private ApplicationUserManager _manager;
        private ApplicationSignInManager _signInManager;
        private IUserService _userService;
        private IAuditTrailService _auditTrailService;
        private IItemService _itemService;
        

        public CustomerController(){}
        
        public CustomerController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, IUserService userService, IAuditTrailService auditTrailService, IItemService itemService)
        {
            _manager = userManager;
            _signInManager = signInManager;
            _userService = userService;
            _auditTrailService = auditTrailService;
            _itemService = itemService;
        }

        // public ApplicationSignInManager SignInManager
        // {
        //     get
        //     {
        //         return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
        //     }
        //     private set 
        //     { 
        //         _signInManager = value; 
        //     }
        // }
        // public ApplicationUserManager UserManager
        // {
        //     get
        //     {
        //         return _manager ?? Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
        //     }
        //     set
        //     {
        //         _manager = value;
        //     }
        // }
        // public IAuditTrailService AuditTrailService
        // {
        //     get
        //     {
        //         return _auditTrailService ?? new AuditTrailServiceImpl();
        //     }
        //     set
        //     {
        //         _auditTrailService = value;
        //     }
        // }
        //
        // public IItemService ItemService
        // {
        //     get
        //     {
        //         return _itemService ?? new ItemServiceImpl();
        //     }
        //     set
        //     {
        //         _itemService = value;
        //     }
        // }
        //
        // public IUserService UserService
        // {
        //     get
        //     {
        //         return _userService ?? new UserServiceImpl();
        //     }
        //     set
        //     {
        //         _userService = value;
        //     }
        // }
        
        // GET
        [AllowAnonymous]
        public ActionResult CreateCustomer()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> CreateCustomer(CustomerViewModel customerViewModel)
        {

            var resultSet = await _userService.CreateUser(customerViewModel);

            if (resultSet.Item1 != null)
            {
                if (resultSet.Item2)
                {
                    await _signInManager.SignInAsync(resultSet.Item1, isPersistent:false, rememberBrowser:false);
                    _auditTrailService.CreateAuditTrail(resultSet.Item1.Id, "User Logged In");
                    return RedirectToAction("HomePage");
                }
            }else if (resultSet.Item1 == null && resultSet.Item3 != null)
            {
                ViewBag.HasError = true;
                ViewBag.Error = resultSet.Item3;
            }

            return View();
        }

        
        public ActionResult HomePage()
        {
            var userId = Current.User.Identity.GetUserId();
            var items = _itemService.GetAllItems();
            _itemService.OpenCart(userId);
            _auditTrailService.CreateAuditTrail(userId, "Queried all Items");
            return View(items);
        }

        public ActionResult OpenCartPage()
        {
            var userId = Current.User.Identity.GetUserId();
            var cart = _itemService.GetUserCart(userId);
            return View(cart);
        }

        public ActionResult AddToCart(int itemId)
        {
            var userId = Current.User.Identity.GetUserId();
            _itemService.AddToCart(userId, itemId);
            
            return View("HomePage", _itemService.GetAllItems());
        }
        
        public ActionResult IncreaseQuantity(int itemId)
        {
            var userId = Current.User.Identity.GetUserId();
            _itemService.IncreaseQuantity(userId, itemId);
            var cart = _itemService.GetUserCart(userId);
            return View("OpenCartPage",cart);
        }
        public ActionResult DecreaseQuantity(int itemId)
        {
            var userId = Current.User.Identity.GetUserId();
            _itemService.DecreaseQuantity(userId, itemId);
            var cart = _itemService.GetUserCart(userId);
            return View("OpenCartPage",cart);
        }

        public async Task<ActionResult> Checkout()
        {
            var userId = Current.User.Identity.GetUserId();
            var invoice = _itemService.Checkout(userId);
            var user = await _manager.FindByIdAsync(userId);
            
            //send mail with invoice info

            var mail = $"Your Order Has Been Processed, Total Price is {invoice.TotalPrice}";
            // var document = PdfCreator.CreatePdf(invoice);
            // Attachment attachment = new Attachment(document);
            await NotificationRequest.SendMail(user.Email, mail, "Order Invoice");

            return View("HomePage", _itemService.GetAllItems());
        }
    }
}