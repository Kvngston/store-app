﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;
using StoreApp.Models;

namespace StoreApp.Jobs
{
    public static class PurgeExpiredItems
    {
        public static void Purge()
        {
            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
            var scheduler =  schedulerFactory.GetScheduler().GetAwaiter().GetResult();

            IJobDetail jobDetail = JobBuilder.Create<PurgeJob>().WithIdentity("Purge Items").Build();
            //everyday cron expression 
            ITrigger trigger = TriggerBuilder.Create().ForJob(jobDetail).WithCronSchedule("0 0 0 * * ?")
                .WithIdentity("Purge Items").StartNow().Build();

            scheduler.ScheduleJob(jobDetail, trigger);
            scheduler.Start();
        }
    }

    public class PurgeJob : IJob
    {


        private ApplicationDbContext db;
        
        public PurgeJob(ApplicationDbContext dbContext)
        {
            db = dbContext;
        }
        
        public Task Execute(IJobExecutionContext context)
        {
            var items = db.Items.ToList();

            var expiredOnes = items
                .Where(item => item.ExpiryDate.CompareTo(DateTime.Now) < 0 || item.ExpiryDate.CompareTo(DateTime.Now) == 0).ToList();

            db.Items.RemoveRange(expiredOnes);

            var suppliers = db.Suppliers.ToList();
            suppliers.ForEach(supplier =>
            {
                var stillValid = supplier.SupplierItems.Where(item => item.ExpiryDate.CompareTo(DateTime.Now) > 0).ToList();
                supplier.SupplierItems.Clear();
                supplier.SupplierItems = stillValid;

                db.Entry(supplier).State = EntityState.Modified;
                db.SaveChanges();
            });
            return db.SaveChangesAsync();
        }
    }
    
}