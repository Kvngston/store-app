﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Ninject;
using StoreApp.Domains;
using StoreApp.Infrastructure;
using StoreApp.Service;
using StoreApp.Service.ServiceImpl;

namespace StoreApp.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        
        
        
        public DbSet<Cart> Carts { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<AuditTrail> AuditTrails { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<Order> Orders { get; set; }
        
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Staff> Staffs { get; set; }
        
        
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            Database.SetInitializer(new InitializationStrategy());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }

    public class InitializationStrategy : DropCreateDatabaseAlways<ApplicationDbContext>
    {

        private IItemService _itemService;
        private ApplicationUserManager _applicationUserManager;
        private RoleManager<IdentityRole> _roleManager;

        public InitializationStrategy()
        {
            IKernel kernel = new StandardKernel();
            NinjectDependencyResolver ninjectDependencyResolver = new NinjectDependencyResolver(kernel);
            
            _itemService = ninjectDependencyResolver.GetService<IItemService>();
            _applicationUserManager = ninjectDependencyResolver.GetService<ApplicationUserManager>();
            _roleManager = ninjectDependencyResolver.GetService<RoleManager<IdentityRole>>();
        }
        
        public InitializationStrategy(IItemService itemService, ApplicationUserManager userManager, RoleManager<IdentityRole> roleManager)
        {
            _itemService = itemService;
            _applicationUserManager = userManager;
            _roleManager = roleManager;
        }

        protected override void Seed(ApplicationDbContext context)
        {

            var admin = new ApplicationUser {UserName = "admin@admin.com", Email = "admin@admin.com"};
            _applicationUserManager.Create(admin, "Admin1234_");

            var adminRole = new IdentityRole()
            {
                Name = "ADMIN"
            };

            _roleManager.Create(adminRole);
            _applicationUserManager.AddToRole(admin.Id, "ADMIN");
            
            //Seeding Items
            _itemService.SeedInitialItems();
            base.Seed(context);
        }
    }
}