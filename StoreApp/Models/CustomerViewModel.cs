﻿using System.ComponentModel.DataAnnotations;

namespace StoreApp.Models
{
    public class CustomerViewModel
    {
        [Required(ErrorMessage = "First Name Cannot be Null")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        [Required(ErrorMessage = "Address is needed")]
        [StringLength(200, MinimumLength = 8)]
        public string Address { get; set; }

        [Required(ErrorMessage = "Email Cannot be Null")]
        [EmailAddress]
        public string Email { get; set; }

        public string PhoneNumber { get; set; }
        
        public string UserName { get; set; }
        
        [Required(ErrorMessage = "Password not Valid")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm Password cannot be null")]
        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Password Mismatch")]
        public string ConfirmPassword { get; set; }
    }
}