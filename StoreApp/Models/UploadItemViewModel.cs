﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace StoreApp.Models
{
    public class UploadItemViewModel
    {
        [Required(ErrorMessage = "Item Name Cannot be Null")]
        [Display(Name = "Item Name")]
        public string ItemName { get; set; }
        
        [Required(ErrorMessage = "Item Price Cannot be Null")]
        [Display(Name = "Item Price")]
        public decimal ItemPrice { get; set; }
        
        [Required(ErrorMessage = "Item Quantity Cannot be Null")]
        [Display(Name = "Quantity")]
        public int Quantity { get; set; }

        [Required]
        [Display(Name = "Item Image")]
        public HttpPostedFileBase File { get; set; }

        [Required(ErrorMessage = "Expiry Date Cannot be Null")]
        [Display(Name = "Expiry Date")]
        public DateTime ExpiryDate { get; set; }
        
        [Required(ErrorMessage = "Category Cannot be Null")]
        [Display(Name = "Category")]
        public string Category { get; set; }
    }
}