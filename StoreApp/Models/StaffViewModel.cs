﻿using System.ComponentModel.DataAnnotations;

namespace StoreApp.Models
{
    public class StaffViewModel
    {
        
        [Required(ErrorMessage = "First Name Cannot be Null")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        [Required(ErrorMessage = "Email Cannot be Null")]
        [EmailAddress]
        public string Email { get; set; }

        public string PhoneNumber { get; set; }
    }
}