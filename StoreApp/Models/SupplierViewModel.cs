﻿using System.ComponentModel.DataAnnotations;

namespace StoreApp.Models
{
    public class SupplierViewModel
    {
        [Required(ErrorMessage = "First Name Cannot be Null")]
        [Display(Name = "Supplier Name")]
        public string SupplierName { get; set; }
        
        [Required(ErrorMessage = "Email Cannot be Null")]
        [EmailAddress]
        public string Email { get; set; }

        public string PhoneNumber { get; set; }
        
        [Required(ErrorMessage = "Password not Valid")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm Password cannot be null")]
        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Password Mismatch")]
        public string ConfirmPassword { get; set; }
    }
}